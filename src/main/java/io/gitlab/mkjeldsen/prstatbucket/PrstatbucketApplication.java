package io.gitlab.mkjeldsen.prstatbucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrstatbucketApplication {

    public static void main(final String[] args) {
        SpringApplication.run(PrstatbucketApplication.class, args);
    }
}

package io.gitlab.mkjeldsen.prstatbucket;

import io.gitlab.mkjeldsen.prstatbucket.apimodel.MaybeWrappedUuid;
import java.sql.Types;
import java.util.UUID;
import org.jdbi.v3.core.argument.AbstractArgumentFactory;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.argument.Arguments;
import org.jdbi.v3.core.config.ConfigRegistry;

public final class MaybeWrappedUuidArgumentFactory
        extends AbstractArgumentFactory<MaybeWrappedUuid> {
    public MaybeWrappedUuidArgumentFactory() {
        super(Types.OTHER);
    }

    @Override
    protected Argument build(MaybeWrappedUuid value, ConfigRegistry config) {
        var arguments = config.get(Arguments.class);
        var argument = arguments.findFor(UUID.class, value.value());
        assert argument.isPresent() : "always present, even when non-functional";
        return argument.get();
    }
}

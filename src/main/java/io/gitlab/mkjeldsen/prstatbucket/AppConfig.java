package io.gitlab.mkjeldsen.prstatbucket;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import io.gitlab.mkjeldsen.prstatbucket.duration.CachingDurationDensityEstimateService;
import io.gitlab.mkjeldsen.prstatbucket.duration.DurationDensityEstimateDao;
import io.gitlab.mkjeldsen.prstatbucket.duration.DurationDensityEstimateService;
import io.gitlab.mkjeldsen.prstatbucket.duration.DurationDensityReport;
import io.gitlab.mkjeldsen.prstatbucket.duration.StartEndRecord;
import io.gitlab.mkjeldsen.prstatbucket.unresolved.UnresolvedReviewDao;
import io.gitlab.mkjeldsen.prstatbucket.unresolved.UnresolvedReviewService;
import java.nio.file.Path;
import java.time.Clock;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ForkJoinPool;
import javax.sql.DataSource;
import org.flywaydb.core.api.ErrorCode;
import org.flywaydb.core.api.FlywayException;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.postgres.PostgresPlugin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

@Configuration(proxyBeanMethods = false)
public class AppConfig {

    private final Environment env;

    public AppConfig(Environment env) {
        this.env = env;
    }

    @Bean("jsonProvider")
    @Profile("!cache")
    public JsonSupplier liveJsonProvider() {
        return new LiveJsonSupplier(
                env.getRequiredProperty("api.client-id"), env.getRequiredProperty("api.secret"));
    }

    @Bean("jsonProvider")
    @Profile("cache")
    public JsonSupplier recordingJsonProvider(@Value("${cache-path}") String cachePath) {
        return new CachingJsonSupplier(
                Path.of(cachePath),
                new LiveJsonSupplier(
                        env.getRequiredProperty("api.client-id"),
                        env.getRequiredProperty("api.secret")));
    }

    @Bean
    public FlywayMigrationStrategy flywayMigrationStrategy() {
        return flyway -> {
            try {
                flyway.migrate();
            } catch (FlywayException e) {
                if (e.getErrorCode() == ErrorCode.VALIDATE_ERROR) {
                    flyway.repair();
                }
                flyway.migrate();
            }
        };
    }

    @Bean
    public Jdbi jdbi(DataSource dataSource) {
        return Jdbi.create(dataSource)
                .installPlugin(new PostgresPlugin())
                .registerArgument(new MaybeWrappedUuidArgumentFactory());
    }

    @Bean
    @ConfigurationProperties("repositories")
    public Collection<String> repositories() {
        // This bean is basically a hack.
        //
        // Lists are flattened to indexed key-value pairs. When two profiles
        // define the same list with a different number of entries,
        // Environment::getProperty does not consider one profile's definition
        // to override the other's, thereby sourcing entries from both lists.
        // Spring's own configuration magic knows how to override, sourcing only
        // from the "highest priority" (seemingly the last) profile.
        //
        // This bean exists to provide the correct repository list override. It
        // is then fed into "repositoryUrls" to be turned into a collection of
        // URLs; because we don't want users to have to deal with that. As a
        // bonus, a client can now access the repository full-name list
        // directly, should they want to.
        //
        // Also, set is more apt but list is cheaper. Just don't repeat
        // repositories.
        return new ArrayList<>();
    }

    @Bean
    public Collection<String> repositoryUrls(Collection<String> repositories) {

        final var repos = new ArrayList<String>(repositories.size());

        for (var repo : repositories) {
            final var url =
                    "https://api.bitbucket.org/2.0/repositories/"
                            + repo
                            + "/pullrequests?state=OPEN&state=MERGED&state=DECLINED";
            repos.add(url);
        }

        return List.copyOf(repos);
    }

    @Bean
    public ForkJoinPool executor() {
        return new ForkJoinPool();
    }

    @Bean
    public Ingester ingester(ForkJoinPool executor, Jdbi jdbi, JsonSupplier jsonSupplier) {
        return new BackgroundIngester(PullRequestStateFilter.all(), jsonSupplier, jdbi, executor);
    }

    @Bean
    public Ingester unresolvedPrIngester(
            ForkJoinPool executor, Jdbi jdbi, JsonSupplier jsonSupplier) {
        return new BackgroundIngester(PullRequestStateFilter.open(), jsonSupplier, jdbi, executor);
    }

    @Bean
    public Clock clock() {
        return Clock.systemUTC();
    }

    @Bean
    public UnresolvedReviewService unresolvedReviewDao(Clock clock, Jdbi jdbi) {
        return new UnresolvedReviewDao(jdbi, clock);
    }

    @Bean
    public DurationDensityEstimateService durationDensityEstimateService(Jdbi jdbi) {
        return new DurationDensityEstimateDao(jdbi);
    }

    @Bean
    public DurationDensityEstimateService cachingDurationDensityEstimateService(
            LoadingCache<DurationDensityReport, Collection<StartEndRecord>> cache) {
        return new CachingDurationDensityEstimateService(cache);
    }

    @Bean
    public LoadingCache<DurationDensityReport, Collection<StartEndRecord>> durationDensityCache(
            DurationDensityEstimateService durationDensityEstimateService) {
        return Caffeine.newBuilder()
                .expireAfterWrite(Duration.ofMinutes(10))
                .maximumSize(1_000)
                .build(durationDensityEstimateService::dataFor);
    }

    @Bean
    public Cache<DurationDensityReport, Long> lastModifiedCache() {
        return Caffeine.newBuilder()
                .expireAfterWrite(Duration.ofMinutes(10))
                .maximumSize(1_000)
                .build();
    }
}

package io.gitlab.mkjeldsen.prstatbucket.unresolved;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Duration;

public record UnresolvedReview(
        String url,
        String destination,
        String title,
        @JsonIgnore Duration rawAge,
        String age,
        int commentCount,
        int taskCount,
        int approvalCount)
        implements Comparable<UnresolvedReview> {

    public String getTitle() {
        return title;
    }

    public String getAge() {
        return age;
    }

    public String getDestination() {
        return destination;
    }

    public String getUrl() {
        return url;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public int getApprovalCount() {
        return approvalCount;
    }

    @Override
    public int compareTo(UnresolvedReview other) {
        // Older pull requests are higher priority than newer pull requests but
        // shorter durations are "less than" longer durations, so invert order.
        int res = rawAge.compareTo(other.rawAge) * -1;
        if (res == 0) {
            res = title.compareTo(other.title);
        }
        return res;
    }
}

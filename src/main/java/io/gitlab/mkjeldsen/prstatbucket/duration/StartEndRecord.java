package io.gitlab.mkjeldsen.prstatbucket.duration;

public record StartEndRecord(long start, long end) {}

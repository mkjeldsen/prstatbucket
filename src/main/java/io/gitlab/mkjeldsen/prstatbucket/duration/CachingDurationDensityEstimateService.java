package io.gitlab.mkjeldsen.prstatbucket.duration;

import com.github.benmanes.caffeine.cache.LoadingCache;
import java.util.Collection;

public final class CachingDurationDensityEstimateService implements DurationDensityEstimateService {

    private final LoadingCache<DurationDensityReport, Collection<StartEndRecord>> cache;

    public CachingDurationDensityEstimateService(
            final LoadingCache<DurationDensityReport, Collection<StartEndRecord>> cache) {
        this.cache = cache;
    }

    @Override
    public Collection<StartEndRecord> dataFor(final DurationDensityReport report) {
        return cache.get(report);
    }
}

package io.gitlab.mkjeldsen.prstatbucket.duration;

import java.util.Collection;

public interface DurationDensityEstimateService {
    Collection<StartEndRecord> dataFor(final DurationDensityReport report);
}

package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record Destination(
        @JsonProperty("commit") Commit commit,
        @JsonProperty("repository") Repository repository,
        @JsonProperty("branch") Branch branch) {}

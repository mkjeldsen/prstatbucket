package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;

@JsonIgnoreProperties(ignoreUnknown = true)
public record PullRequest(
        @JsonProperty("description") String description,
        @JsonProperty("links") Links links,
        @JsonProperty("title") String title,
        @JsonProperty("close_source_branch") boolean closeSourceBranch,
        @JsonProperty("type") String type,
        @JsonProperty("id") int id,
        @JsonProperty("destination") Destination destination,
        @JsonProperty("created_on") Instant createdOn,
        @JsonProperty("summary") Summary summary,
        @JsonProperty("source") Source source,
        @Deprecated @JsonProperty("comment_count") int commentCount,
        @JsonProperty("state") State state,
        @JsonProperty("task_count") int taskCount,
        @JsonProperty("reason") String reason,
        @JsonProperty("updated_on") Instant updatedOn,
        @JsonProperty("author") User author,
        @JsonProperty("merge_commit") MergeCommit mergeCommit,
        @JsonProperty("closed_by") ClosedBy closedBy) {

    /** This value cannot be relied upon and must be calculated manually. */
    @Deprecated
    @Override
    public int commentCount() {
        return commentCount;
    }

    public enum State {
        MERGED,
        SUPERSEDED,
        OPEN,
        DECLINED,
        @JsonEnumDefaultValue
        UNKNOWN;
    }
}

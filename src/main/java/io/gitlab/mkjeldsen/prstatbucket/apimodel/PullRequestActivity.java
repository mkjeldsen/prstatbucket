package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.jspecify.annotations.Nullable;

@JsonIgnoreProperties(ignoreUnknown = true)
public record PullRequestActivity(
        String url,
        @Nullable String next,
        @Nullable Instant closedTs,
        List<Comment> comments,
        List<Approval> approvals) {

    public PullRequestActivity {
        comments = List.copyOf(comments);
        approvals = List.copyOf(approvals);
    }

    public Optional<String> getNext() {
        return Optional.ofNullable(next());
    }

    public Optional<String> getPrevious() {
        return Optional.empty();
    }
}

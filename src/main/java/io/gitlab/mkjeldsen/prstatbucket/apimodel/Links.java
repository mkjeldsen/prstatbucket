package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record Links(
        @JsonProperty("decline") Link decline,
        @JsonProperty("commits") Link commits,
        @JsonProperty("self") Link self,
        @JsonProperty("comments") Link comments,
        @JsonProperty("merge") Link merge,
        @JsonProperty("html") Link html,
        @JsonProperty("activity") Link activity,
        @JsonProperty("diff") Link diff,
        @JsonProperty("approve") Link approve,
        @JsonProperty("statuses") Link statuses) {}

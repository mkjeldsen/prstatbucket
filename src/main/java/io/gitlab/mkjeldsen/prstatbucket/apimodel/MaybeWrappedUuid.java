package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import java.util.UUID;

public record MaybeWrappedUuid(UUID value) {

    public MaybeWrappedUuid(String value) {
        this(UUID.fromString(unwrap(value)));
    }

    private static String unwrap(String value) {
        int beginIndex = 0;
        if (value.charAt(0) == '{') {
            ++beginIndex;
        }
        int endIndex = value.length();
        if (value.charAt(endIndex - 1) == '}') {
            --endIndex;
        }
        var unwrapped = value.substring(beginIndex, endIndex);
        assert unwrapped.length() == 36 : value + " -> " + unwrapped;
        return unwrapped;
    }
}

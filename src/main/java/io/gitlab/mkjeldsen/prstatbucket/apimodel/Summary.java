package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record Summary(
        @JsonProperty("raw") String raw,
        @JsonProperty("markup") String markup,
        @JsonProperty("html") String html,
        @JsonProperty("type") String type) {}

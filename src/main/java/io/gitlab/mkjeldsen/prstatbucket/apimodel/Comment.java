package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;

@JsonIgnoreProperties(ignoreUnknown = true)
public record Comment(
        String url,
        String content,
        @JsonProperty("created_on") Instant createdOn,
        boolean deleted,
        User author) {}

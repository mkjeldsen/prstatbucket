package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record Commit(
        @JsonProperty("hash") String hash,
        @JsonProperty("type") String type,
        @JsonProperty("links") CommitLinks links) {}

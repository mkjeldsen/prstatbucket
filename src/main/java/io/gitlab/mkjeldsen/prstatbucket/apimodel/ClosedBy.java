package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record ClosedBy(
        @JsonProperty("username") String username,
        @JsonProperty("display_name") String displayName,
        @JsonProperty("account_id") String accountId,
        @JsonProperty("links") ProfileLinks links,
        @JsonProperty("nickname") String nickname,
        @JsonProperty("type") String type,
        @JsonProperty("uuid") MaybeWrappedUuid uuid) {}

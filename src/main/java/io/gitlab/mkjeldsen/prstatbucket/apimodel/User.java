package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.jspecify.annotations.Nullable;

@JsonIgnoreProperties(ignoreUnknown = true)
public record User(
        @JsonProperty("username") String username,
        @JsonProperty("display_name") String displayName,
        @JsonProperty("account_id") String accountId,
        @JsonProperty("links") ProfileLinks links,
        @JsonProperty("nickname") String nickname,
        @JsonProperty("type") String type,
        @JsonProperty("uuid") MaybeWrappedUuid uuid) {

    public static final User DELETED =
            new User(
                    "<deleted>",
                    "<deleted>",
                    "<deleted>",
                    new ProfileLinks(
                            new Link("https://deleted.example"),
                            new Link("https://deleted.example"),
                            new Link("https://deleted.example")),
                    "<deleted>",
                    "<deleted>",
                    new MaybeWrappedUuid("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"));

    @Override
    public boolean equals(final @Nullable Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof User user)) {
            return false;
        }
        return uuid.equals(user.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}

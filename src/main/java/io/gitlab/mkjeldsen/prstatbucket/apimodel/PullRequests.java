package io.gitlab.mkjeldsen.prstatbucket.apimodel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import java.util.Optional;
import org.jspecify.annotations.Nullable;

@JsonIgnoreProperties(ignoreUnknown = true)
public record PullRequests(
        @JsonProperty("pagelen") int pagelen,
        @JsonProperty("values") List<PullRequest> values,
        @JsonProperty("page") int page,
        @JsonProperty("size") int size,
        @JsonProperty("next") @Nullable String next,
        @JsonProperty("previous") @Nullable String previous) {

    public PullRequests {
        values = List.copyOf(values);
    }

    public Optional<String> getNext() {
        return Optional.ofNullable(next());
    }

    public Optional<String> getPrevious() {
        return Optional.ofNullable(previous());
    }
}

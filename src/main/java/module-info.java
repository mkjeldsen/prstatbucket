import org.jspecify.annotations.NullMarked;

@NullMarked
module io.gitlab.mkjeldsen.prstatbucket {
    exports io.gitlab.mkjeldsen.prstatbucket;
    exports io.gitlab.mkjeldsen.prstatbucket.apimodel;
    exports io.gitlab.mkjeldsen.prstatbucket.duration;
    exports io.gitlab.mkjeldsen.prstatbucket.index;
    exports io.gitlab.mkjeldsen.prstatbucket.unresolved;

    opens io.gitlab.mkjeldsen.prstatbucket to
            spring.core;
    opens io.gitlab.mkjeldsen.prstatbucket.db.migration;
    opens io.gitlab.mkjeldsen.prstatbucket.duration to
            spring.core;
    opens io.gitlab.mkjeldsen.prstatbucket.freemarkertemplates;
    opens io.gitlab.mkjeldsen.prstatbucket.index to
            spring.core;
    opens io.gitlab.mkjeldsen.prstatbucket.unresolved to
            spring.core;

    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.datatype.jsr310;
    requires com.github.benmanes.caffeine;
    requires com.zaxxer.hikari;
    requires freemarker;
    requires jakarta.servlet;
    requires java.sql;
    requires org.flywaydb.core;
    requires org.jdbi.v3.core;
    requires org.jdbi.v3.postgres;
    requires org.jspecify;
    requires org.postgresql.jdbc;
    requires org.slf4j;
    requires scribejava.core;
    requires scribejava.httpclient.okhttp;
    requires spring.beans;
    requires spring.boot.actuator;
    requires spring.boot.autoconfigure;
    requires spring.boot;
    requires spring.context;
    requires spring.core;
    requires spring.web;
    requires spring.webmvc;
}

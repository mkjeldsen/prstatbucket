import org.jspecify.annotations.NullMarked;

@NullMarked
open module io.gitlab.mkjeldsen.prstatbucket.t {
    requires com.fasterxml.jackson.databind;
    requires io.gitlab.mkjeldsen.prstatbucket;
    requires java.sql;
    requires net.bytebuddy.agent;
    requires net.bytebuddy;
    requires net.javacrumbs.jsonunit.spring;
    requires org.assertj.core;
    requires org.hamcrest;
    requires org.jdbi.v3.core;
    requires org.jdbi.v3.postgres;
    requires org.jspecify;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.params;
    requires org.mockito;
    requires spring.beans;
    requires spring.boot.test.autoconfigure;
    requires spring.boot.test;
    requires spring.context;
    requires spring.test;
    requires spring.web;
    requires testcontainers;
}

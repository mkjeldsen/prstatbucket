package io.gitlab.mkjeldsen.prstatbucket.t.testhelper;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import org.jspecify.annotations.Nullable;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.testcontainers.lifecycle.Startables;

final class TestcontainersInitializer
        implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private static final List<TestcontainerPropertyValuesProvider> propertyValuesProviders =
            List.of(new ComposeTestcontainerPropertyValuesProvider());

    private static final CompletableFuture<@Nullable Void> startingProviders;

    static {
        var startables =
                propertyValuesProviders.stream()
                        .map(TestcontainerPropertyValuesProvider::getContainer);
        startingProviders = Startables.deepStart(startables);
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        startingProviders.join();
        var environment = applicationContext.getEnvironment();
        for (var propertyValuesProvider : propertyValuesProviders) {
            propertyValuesProvider.getPropertyValues().applyTo(environment);
        }
    }
}

package io.gitlab.mkjeldsen.prstatbucket.t.testhelper;

import java.io.File;
import org.springframework.boot.test.util.TestPropertyValues;
import org.testcontainers.containers.ComposeContainer;
import org.testcontainers.lifecycle.Startable;

final class ComposeTestcontainerPropertyValuesProvider
        implements TestcontainerPropertyValuesProvider {

    private final ComposeContainer compose;

    public ComposeTestcontainerPropertyValuesProvider() {
        compose =
                new ComposeContainer(new File("docker-compose.yml"))
                        .withLocalCompose(false)
                        .withExposedService("db", 5432);
    }

    @Override
    public Startable getContainer() {
        return compose;
    }

    @Override
    public TestPropertyValues getPropertyValues() {
        var host = compose.getServiceHost("db", 5432);
        var port = compose.getServicePort("db", 5432);
        return TestPropertyValues.of("db.server=" + host, "db.port=" + port);
    }
}

package io.gitlab.mkjeldsen.prstatbucket.t.testhelper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.junit.jupiter.api.Tag;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;

/**
 * A test group that enables and relies on a Testcontainers Compose module integration to
 * automatically spawn external dependencies.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ContextConfiguration(initializers = TestcontainersInitializer.class)
@TestPropertySource("/io/gitlab/mkjeldsen/prstatbucket/t/config/test.properties")
@Tag("compose")
public @interface Compose {}

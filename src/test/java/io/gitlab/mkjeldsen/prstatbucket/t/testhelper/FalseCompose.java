package io.gitlab.mkjeldsen.prstatbucket.t.testhelper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A {@code @FalseCompose} is a test group that is not a {@link Compose @Compose} group yet has the
 * same prerequisite; usually implicitly. It exists because the application configuration presently
 * is not sophisticated enough to sidestep those dependencies and accomplishing that unlocks
 * comparatively few, low-criticality tests.
 */
@Compose
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface FalseCompose {}

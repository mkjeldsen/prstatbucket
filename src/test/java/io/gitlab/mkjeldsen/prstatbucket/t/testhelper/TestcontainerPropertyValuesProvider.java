package io.gitlab.mkjeldsen.prstatbucket.t.testhelper;

import org.springframework.boot.test.util.TestPropertyValues;
import org.testcontainers.lifecycle.Startable;

interface TestcontainerPropertyValuesProvider {

    Startable getContainer();

    TestPropertyValues getPropertyValues();
}
